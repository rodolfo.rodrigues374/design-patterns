package com.solid.solid.isp;

public class Quadrado implements NaoRedondo {

	@Override
	public void Area() {
		System.out.println("Area");
	}

	@Override
	public void Perimetro() {
		System.out.println("Perimetro");
	}

}
