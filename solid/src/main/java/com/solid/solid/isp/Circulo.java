package com.solid.solid.isp;

public class Circulo implements Redondo {

	@Override
	public void Area() {
		System.out.println("Area");
	}

	@Override
	public void circunferencia() {
		System.out.println("Circuferencia");
	}

	@Override
	public void Raio() {
		System.out.println("Raio");		
	}

}
