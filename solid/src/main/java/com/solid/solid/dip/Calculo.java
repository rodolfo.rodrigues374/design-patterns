package com.solid.solid.dip;

import java.util.List;

public class Calculo implements ICalculo {

	private IProduto produto;

	@Override
	public void calcularPrecoPedido(IProduto produto) {
		this.produto = produto;

	}

	@Override
	public double total() {

		List<Produto> lista = this.produto.obterProdutos();

		for (Produto produto : lista) {
			System.out.println(produto.getPedidoId());
		}

		return 45;
	}

}
