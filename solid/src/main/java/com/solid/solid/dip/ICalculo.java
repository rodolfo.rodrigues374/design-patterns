package com.solid.solid.dip;

public interface ICalculo {
	
	void calcularPrecoPedido(IProduto produto);
	double total();
}
