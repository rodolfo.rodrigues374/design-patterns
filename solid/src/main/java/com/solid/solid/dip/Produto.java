package com.solid.solid.dip;

import java.util.ArrayList;
import java.util.List;

public class Produto implements IProduto {

	private Integer pedidoId;

	public Integer getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(Integer pedidoId) {
		this.pedidoId = pedidoId;
	}

	public void setarPedidoId(int pedidoId) {
		System.out.println("Seleciona o produto");
		this.pedidoId = pedidoId;
	}

	public List<Produto> obterProdutos() {
		return this.consultarNoBanco(this.pedidoId);
	}

	private List<Produto> consultarNoBanco(Integer pedidoId) {
		
		Produto p = new Produto();
		p.setPedidoId(pedidoId);
	
		ArrayList<Produto> lista = new ArrayList<>();
		lista.add(p);

		return lista;
	}
}
