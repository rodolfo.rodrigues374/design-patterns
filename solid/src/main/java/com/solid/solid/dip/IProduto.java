package com.solid.solid.dip;

import java.util.List;

public interface IProduto {

	void setarPedidoId(int pedidoId);
	List<Produto> obterProdutos();
}
