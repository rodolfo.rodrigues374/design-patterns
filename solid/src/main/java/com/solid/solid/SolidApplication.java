package com.solid.solid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.solid.solid.dip.Calculo;
import com.solid.solid.dip.NotaFiscal;
import com.solid.solid.dip.Produto;
import com.solid.solid.lsp.ArquivoReadOnly;
import com.solid.solid.lsp.ArquivoWriteOnly;
import com.solid.solid.lsp.ExecutarArquivo;
import com.solid.solid.lsp.IArquivoEscrever;
import com.solid.solid.lsp.IArquivoLer;
import com.solid.solid.lsp.IArquivos;
import com.solid.solid.lsp.ArquivoTexto;

@SpringBootApplication
public class SolidApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolidApplication.class, args);
		
		
		//OPC
//		ArrayList<Arquivo> lista = new ArrayList<>();
//		
//		ArquivoPdf pdf = new ArquivoPdf();
//		ArquivoWord word = new ArquivoWord(); 
//		ArquivoTxt txt = new ArquivoTxt();
//		
//		lista.add(pdf);
//		lista.add(word);
//		lista.add(txt);
//		
//		
//		GeradorArquivos gerar =  new GeradorArquivos();
//		
//		gerar.Gerador(lista);
		
		
		
		//LSP
		
//		ExecutarArquivo exec = new ExecutarArquivo();
//		
//		
//		IArquivoEscrever escrita = new ArquivoWriteOnly();					
//		exec.executar(escrita);
//		
//		IArquivoLer leitura = new ArquivoReadOnly();					
//		exec.executar(leitura);
//	
//		
//		ArquivoTexto texto = new ArquivoTexto();
//		exec.executar(texto);
//		
//		IArquivos texto2 = new ArquivoTexto();
//		exec.executar(texto2);

		
		
		
		
		//DIP
		
		Produto produto = new Produto();
		produto.setarPedidoId(10);
		
		Calculo calculo = new Calculo();
		calculo.calcularPrecoPedido(produto);
		
		NotaFiscal notafiscal = new NotaFiscal();
		notafiscal.gerarNota(calculo);
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
