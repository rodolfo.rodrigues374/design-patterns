package com.solid.solid.lsp;

public class ExecutarArquivo {

	public void executar(IArquivos arq) {

		if (arq instanceof IArquivoEscrever) {
			((IArquivoEscrever) arq).escrever();
		}

		if (arq instanceof IArquivoLer) {
			((IArquivoLer) arq).ler();
		}
	}
}
