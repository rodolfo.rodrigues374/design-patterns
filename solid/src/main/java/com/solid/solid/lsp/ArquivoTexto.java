package com.solid.solid.lsp;

public class ArquivoTexto implements IArquivoEscrever, IArquivoLer {

	@Override
	public void escrever() {
		System.out.println("Escrevendo");
	}

	@Override
	public void ler() {
		System.out.println("lendo");
	}

}
