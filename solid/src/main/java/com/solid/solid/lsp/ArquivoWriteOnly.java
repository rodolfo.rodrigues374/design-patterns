package com.solid.solid.lsp;

public class ArquivoWriteOnly implements IArquivoEscrever {

	@Override
	public void escrever() {
		System.out.println("Escrevendo");
	}

}
