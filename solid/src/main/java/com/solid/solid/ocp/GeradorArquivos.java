package com.solid.solid.ocp;

import java.util.List;

public class GeradorArquivos {

	public void Gerador(List<Arquivo> lista) {
		
		for (Arquivo arquivo : lista) {
			arquivo.gerar();
		}
	}
}
