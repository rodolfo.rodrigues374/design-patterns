# SOLID

SOLID é um acrônimo para os primeiros cinco princípios de design orientado a objetos (OOD) de Robert C. Martin.

Esses princípios estabelecem práticas que contribuem para o desenvolvimento de software com considerações de manutenção e extensão à medida que o projeto cresce. 

A adoção dessas práticas também pode contribuir para evitar odores de código, refatoração de código e desenvolvimento de software ágil ou adaptável.


SOLID significa:

- S — Single Responsiblity Principle (Princípio da responsabilidade única)

    Uma classe deve ter uma única e somente uma responsabilidade.

    Assim podemos ter uma classe com alta coesão    

- O — Open-Closed Principle (Princípio Aberto-Fechado)

    O código deve ser aberto para extensão e fechado para alteração.

- L — Liskov Substitution Principle (Princípio da substituição de Liskov)

    Objetos devem poder ser substituíveis com instâncias de seus tipos base sem alterar o bom funcionamento do software.

- I — Interface Segregation Principle (Princípio da Segregação da Interface)

    Várias interfaces específica são melhores que uma única interface genérica.

- D — Dependency Inversion Principle (Princípio da inversão da dependência)
    Devemos depender de classes abstratas, não de classes concretas.
