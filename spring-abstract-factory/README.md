# Abstract Factory

O Abstract Factory é um padrão de projeto criacional que permite que você produza famílias de objetos relacionados sem ter que especificar suas classes concretas.

Use o Abstract Factory quando seu código precisa trabalhar com diversas famílias de produtos relacionados,

Aqui foi criado um cenário em que temos 3 processos de cargas(Sangria, Venda e Depósito) e 3 clientes.
Cada cliente pode ter um ou mais processos de cargas, cada carga tem sua regra de negócio específica.

- O Cliente 1 precisa implementar as cargas sangria e venda buscando os dados de uma API.

- O Cliente 2 precisa implementar as cargas sangria e venda buscando os dados de arquivos texto.

- O Cliente 3 precisa implementar a carga de depósito buscando os dados de uma API.

Prós

- Temos classes mais coesas com uma única responsabilidade (SRP).
- Caso precise adicionar uma nova carga ou cliente você pode introduzir novas variantes sem quebrar o código existente (OPC).
- Temos interfaces específicas de cada carga, porque são melhores que uma interface genérica (ISP).

 Contras

 - O código torna-se complicado com tantas interfaces e classes.
