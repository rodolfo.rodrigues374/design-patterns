package com.example.demo.numerario.cliente.cliente2;

import com.example.demo.numerario.carga.ISangria;
import com.example.demo.numerario.carga.IVenda;
import com.example.demo.numerario.cliente.IClienteFactory;

public interface ICliente2Factory extends IClienteFactory {

	ISangria processoSangria();
	
	IVenda processoVenda(); 
}
