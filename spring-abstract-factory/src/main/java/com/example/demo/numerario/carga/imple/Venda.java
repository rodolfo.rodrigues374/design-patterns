package com.example.demo.numerario.carga.imple;

import com.example.demo.numerario.carga.IVenda;
import com.example.demo.numerario.cliente.cliente1.ICliente1Factory;
import com.example.demo.numerario.cliente.cliente2.ICliente2Factory;

public class Venda {
	private IVenda venda;

	public Venda(ICliente1Factory factory) {
		this.venda = factory.processoVenda();
	}
	
	public Venda(ICliente2Factory factory) {
		this.venda = factory.processoVenda();
	}
  
	public void CargaVenda() {
		this.venda.carregarVenda();
	}
}
