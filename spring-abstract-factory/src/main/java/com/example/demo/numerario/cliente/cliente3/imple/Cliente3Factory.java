package com.example.demo.numerario.cliente.cliente3.imple;

import com.example.demo.numerario.carga.IDeposito;
import com.example.demo.numerario.cliente.cliente3.ICliente3Factory;
import com.example.demo.numerario.cliente.cliente3.imple.carga.DepositoCliente3;

public class Cliente3Factory implements ICliente3Factory {

	@Override
	public IDeposito processoDeposito() { 
		return new DepositoCliente3();
	}

}
