package com.example.demo.numerario.carga.imple;

import com.example.demo.numerario.carga.ISangria;
import com.example.demo.numerario.cliente.cliente1.ICliente1Factory;
import com.example.demo.numerario.cliente.cliente2.ICliente2Factory;

public class Sangria {

	private ISangria sangria;

	public Sangria(ICliente1Factory factory) {
		this.sangria = factory.processoSangria();
	}
	
	public Sangria(ICliente2Factory factory) {
		this.sangria = factory.processoSangria();
	}

	public void CargaSangria() {
		this.sangria.carregarSangria();
	}
}
