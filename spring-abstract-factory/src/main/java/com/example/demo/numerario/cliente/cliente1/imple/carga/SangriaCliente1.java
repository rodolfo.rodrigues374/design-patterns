package com.example.demo.numerario.cliente.cliente1.imple.carga;

import org.springframework.stereotype.Service;

import com.example.demo.numerario.carga.ISangria;

@Service
public class SangriaCliente1 implements ISangria {

	@Override
	public void carregarSangria() {
		System.out.println("Sangria API");
	}
}
