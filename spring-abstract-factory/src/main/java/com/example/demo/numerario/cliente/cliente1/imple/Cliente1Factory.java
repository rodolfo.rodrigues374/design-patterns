package com.example.demo.numerario.cliente.cliente1.imple;

import com.example.demo.numerario.carga.ISangria;
import com.example.demo.numerario.carga.IVenda;
import com.example.demo.numerario.cliente.cliente1.ICliente1Factory;
import com.example.demo.numerario.cliente.cliente1.imple.carga.SangriaCliente1;
import com.example.demo.numerario.cliente.cliente1.imple.carga.VendaCliente1;

public class Cliente1Factory implements ICliente1Factory {

	@Override
	public ISangria processoSangria() {
		return new SangriaCliente1();
	}

	@Override
	public IVenda processoVenda() {
		return new VendaCliente1();
	}

}
