package com.example.demo.numerario.carga.imple;

import com.example.demo.numerario.carga.IDeposito;
import com.example.demo.numerario.cliente.cliente3.ICliente3Factory;

public class Deposito {

	private IDeposito deposito;
	
	public Deposito(ICliente3Factory factory) {
		this.deposito = factory.processoDeposito();
	}
	
	public void cargaDeposito() {
		this.deposito.carregarDepositos();
	}
	
}
