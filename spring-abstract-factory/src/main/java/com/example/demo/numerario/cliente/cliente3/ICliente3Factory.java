package com.example.demo.numerario.cliente.cliente3;

import com.example.demo.numerario.carga.IDeposito;
import com.example.demo.numerario.cliente.IClienteFactory;

public interface ICliente3Factory extends IClienteFactory {
	
	IDeposito processoDeposito();
}
