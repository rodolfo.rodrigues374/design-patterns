package com.example.demo.numerario;

import com.example.demo.numerario.carga.imple.Deposito;
import com.example.demo.numerario.carga.imple.Sangria;
import com.example.demo.numerario.carga.imple.Venda;
import com.example.demo.numerario.cliente.IClienteFactory;
import com.example.demo.numerario.cliente.cliente1.ICliente1Factory;
import com.example.demo.numerario.cliente.cliente1.imple.Cliente1Factory;
import com.example.demo.numerario.cliente.cliente2.ICliente2Factory;
import com.example.demo.numerario.cliente.cliente2.imple.Cliente2Factory;
import com.example.demo.numerario.cliente.cliente3.ICliente3Factory;
import com.example.demo.numerario.cliente.cliente3.imple.Cliente3Factory;

public class ConfigFactory {

	public static Sangria executarSangria(IClienteFactory arq) {

		Sangria app = null;

		if (arq instanceof ICliente1Factory) {
			app = new Sangria(new Cliente1Factory());
		}

		if (arq instanceof ICliente2Factory) {
			app = new Sangria(new Cliente2Factory());
		}

		return app;
	}

	public static Venda executarVenda(IClienteFactory arq) {

		Venda app = null;

		if (arq instanceof ICliente1Factory) {
			app = new Venda(new Cliente1Factory());
		}

		if (arq instanceof ICliente2Factory) {
			app = new Venda(new Cliente2Factory());
		}

		return app;
	}

	public static Deposito executarDeposito(IClienteFactory arq) {

		Deposito app = null;

		if (arq instanceof ICliente3Factory) {
			app = new Deposito(new Cliente3Factory());
		}

		return app;
	}

}
