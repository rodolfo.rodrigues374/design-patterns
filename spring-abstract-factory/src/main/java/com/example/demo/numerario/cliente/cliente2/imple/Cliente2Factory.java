package com.example.demo.numerario.cliente.cliente2.imple;

import com.example.demo.numerario.carga.ISangria;
import com.example.demo.numerario.carga.IVenda;
import com.example.demo.numerario.cliente.cliente2.ICliente2Factory;
import com.example.demo.numerario.cliente.cliente2.imple.carga.SangriaCliente2;
import com.example.demo.numerario.cliente.cliente2.imple.carga.VendaCliente2;

public class Cliente2Factory implements ICliente2Factory {

	@Override
	public ISangria processoSangria() {
		return new SangriaCliente2();
	}

	@Override
	public IVenda processoVenda() {
		return new VendaCliente2();
	}

}
