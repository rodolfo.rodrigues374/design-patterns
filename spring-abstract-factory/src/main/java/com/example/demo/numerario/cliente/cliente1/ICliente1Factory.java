package com.example.demo.numerario.cliente.cliente1;

import com.example.demo.numerario.carga.ISangria;
import com.example.demo.numerario.carga.IVenda;
import com.example.demo.numerario.cliente.IClienteFactory;

public interface ICliente1Factory extends IClienteFactory {

	ISangria processoSangria();

	IVenda processoVenda();
}
