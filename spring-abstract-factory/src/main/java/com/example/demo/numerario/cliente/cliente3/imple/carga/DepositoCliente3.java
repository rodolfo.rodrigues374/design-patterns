package com.example.demo.numerario.cliente.cliente3.imple.carga;

import com.example.demo.numerario.carga.IDeposito;

public class DepositoCliente3 implements IDeposito {

	@Override
	public void carregarDepositos() {
		System.out.println("Carga deposito arquivo txt");
	}

}
