package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.numerario.ConfigFactory;
import com.example.demo.numerario.cliente.cliente1.imple.Cliente1Factory;
import com.example.demo.numerario.cliente.cliente2.imple.Cliente2Factory;
import com.example.demo.numerario.cliente.cliente3.imple.Cliente3Factory;

@SpringBootApplication
public class SpringAbstractFactoryApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringAbstractFactoryApplication.class, args);

		
		ConfigFactory.executarSangria(new Cliente1Factory()).CargaSangria();
		ConfigFactory.executarVenda(new Cliente1Factory()).CargaVenda();

		ConfigFactory.executarSangria(new Cliente2Factory()).CargaSangria();
		ConfigFactory.executarVenda(new Cliente2Factory()).CargaVenda();

		ConfigFactory.executarDeposito(new Cliente3Factory()).cargaDeposito();
	}

}
